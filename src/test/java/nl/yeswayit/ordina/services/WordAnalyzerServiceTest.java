package nl.yeswayit.ordina.services;

import nl.yeswayit.ordina.options.WordFrequency;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WordAnalyzerServiceTest {

    @Test
    public void calculateHighestFrequency() {
        String test = "Dit is een dit een dit";
        WordAnalyzerService wordAnalyzerService = new WordAnalyzerService();
        int hoogste = wordAnalyzerService.calculateHighestFrequency(test);
        assertEquals(3, hoogste);
    }

    @Test
    public void calculateHighestFrequencyNull() {
        String test = null;
        WordAnalyzerService wordAnalyzerService = new WordAnalyzerService();
        int hoogste = wordAnalyzerService.calculateHighestFrequency(test);
        assertEquals(0, hoogste);
    }

    @Test
    public void calculateHighestFrequencyEmptyl() {
        String test = " \n \t";
        WordAnalyzerService wordAnalyzerService = new WordAnalyzerService();
        int hoogste = wordAnalyzerService.calculateHighestFrequency(test);
        assertEquals(0, hoogste);
    }

    @Test
    public void calculateHighestFrequencyEqualWords() {
        String test = "Morgen schijnt de zon De zon komt morgen op";
        WordAnalyzerService wordAnalyzerService = new WordAnalyzerService();
        int hoogste = wordAnalyzerService.calculateHighestFrequency(test);
        assertEquals(2, hoogste);
    }

    @Test
    public void calculateFrequencyForWord() {
        String test = "Dit is een dit een dit";
        WordAnalyzerService wordAnalyzerService = new WordAnalyzerService();
        int frequency = wordAnalyzerService.calculateFrequencyForWord(test, "een");
        assertEquals(2, frequency);
    }

    @Test
    public void calculateMostFrequentNOverflow() {
        String test = "Dit is een dit een dit";
        WordAnalyzerService wordAnalyzerService = new WordAnalyzerService();
        List<WordFrequency> wordlist  = wordAnalyzerService.calculateMostFrequentNWords(test, 3);
        assertEquals(3, wordlist.size());
        assertEquals("[(\"dit\", 3), (\"een\", 2), (\"is\", 1)]", wordlist.toString());
    }

    @Test
    public void calculateMostFrequentN() {
        String test = "The sun shines over the lake";
        WordAnalyzerService wordAnalyzerService = new WordAnalyzerService();
        List<WordFrequency> wordlist  = wordAnalyzerService.calculateMostFrequentNWords(test, 3);
        assertEquals(3, wordlist.size());
        assertEquals("[(\"the\", 2), (\"lake\", 1), (\"over\", 1)]", wordlist.toString());
    }




}