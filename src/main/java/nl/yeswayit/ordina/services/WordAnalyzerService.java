package nl.yeswayit.ordina.services;

import nl.yeswayit.ordina.impl.WordFrequencyInfo;
import nl.yeswayit.ordina.options.WordFrequency;
import nl.yeswayit.ordina.options.WordFrequencyAnalyzer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class WordAnalyzerService implements WordFrequencyAnalyzer {

    public int calculateHighestFrequency(String text) {
        if (text == null || text.trim().length() ==0 ) {
            return 0;
        }
        List<WordFrequency> wordlist = orginaizeText(text);
        return wordlist.get(0).getFrequency();
    }

    public int calculateFrequencyForWord(String text, String word) {
        WordFrequencyInfo freqInfo = new WordFrequencyInfo(word);
        Arrays.asList(text.split("\\s")).forEach(input -> {
            if (input.equalsIgnoreCase(word)) {
                freqInfo.incFrequency();
            }
        });
        return freqInfo.getFrequency();
    }

    public List<WordFrequency> calculateMostFrequentNWords(String text, int n) {
        List<WordFrequency> wordlist = orginaizeText(text);
        int maxNum = wordlist.size();
        List<WordFrequency> result;
        if (n < maxNum) {
            result =  wordlist.subList(0, n);
        } else {
            result = wordlist;
        }
        return result;
    }


    private List<WordFrequency> orginaizeText(String text) {
        List<String> wordlist = Arrays.asList(text.split("\\s"));
        final Map<String, WordFrequencyInfo> mapped = new HashMap<String, WordFrequencyInfo>();

        Consumer<String> organizer = input -> {
            String toMap = input.toLowerCase();
            if (mapped.containsKey(toMap)) {
                WordFrequencyInfo existingItem = mapped.get(toMap);
                existingItem.incFrequency();
            } else {
                WordFrequencyInfo newItem = new WordFrequencyInfo(toMap);
                newItem.incFrequency();
                mapped.put(toMap, newItem);
            }
        };
        wordlist.forEach(organizer);
        List<WordFrequency> preList = mapped.values().stream().collect(Collectors.toList());

        preList.sort((u, v) -> {
            if (u.getFrequency() == v.getFrequency()) {
                return String.CASE_INSENSITIVE_ORDER.compare(u.getWord(), v.getWord());
            }
            return u.getFrequency() > v.getFrequency() ? -1 : 1;
        });
        return preList;
    }


}
