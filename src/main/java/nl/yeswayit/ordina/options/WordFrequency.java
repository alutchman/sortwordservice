package nl.yeswayit.ordina.options;

public interface WordFrequency {
    String getWord();
    int getFrequency();
}